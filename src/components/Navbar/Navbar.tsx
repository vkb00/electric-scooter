import './Navbar.scss';
import logo from './../../imgs/logo.svg'
import ButtonBuy from './ButtonBuy/ButtonBuy'
function Navbar() {
    return (
        <nav>
            <img src={logo} alt="" />
            <div className='navItem'>О продукте</div>
            <div className='navItem'>Внешний вид</div>
            <div className='navItem'>Безопасность</div>
            <div className='navItem'>Отзывы</div>
            <div className='navItem'>FAQ</div>
           <ButtonBuy/>
        </nav>

    )
}
export default Navbar;